#####################################################################
Extending the plugins and publishing on your own
#####################################################################

## Install vsce globally
npm intall -g vsce

## Manage extensions
- Login to your market place account using Microsoft account (https://marketplace.visualstudio.com/manage)
- Create a new publisher if you haven't got one
(https://vscode.readthedocs.io/en/latest/extensions/publish-extension/#:~:text=A%20publisher%20is%20an%20identity,json%20file.&text=vsce%20will%20remember%20the%20provided,future%20references%20to%20this%20publisher.)

## Get Personal Access Token
- Login to dev.zaure.com using the same Microsoft account
- Go to your publisher profile https://dev.azure.com/<organization>
- Once you have a origanization name, get personal access token at (https://dev.azure.com/<organization>). Make sure you login with the same account.
- Create a new PAT with Marketplace manage permission.

# Publishing the extension
- Make sure you set the correct publisher name in plackage.json file
- Change the version name and run vsce package to make sure it can build correctly
- vsce publish and then paste your PAT

# Manually uploading 
- vsce package to build a new extension
- Go to your marketplace manage url (https://marketplace.visualstudio.com/manage/publishers/<organization>) and click the three dots (...) that you want to update to
- Drag and drop to upload the new version of the extension