# Change Log

All notable changes to the "mvvm-generator" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.1.2]

- Added Splash view and sample views to start using MVVM

## [0.1.0]

- Fixed main view name not matched with the files

## [0.0.9]

- Made mainview as entry point


## [0.0.8]

- Support for Flutter 2.0

## [0.0.7]

- Added immutable to suppress warnings

## [0.0.6]

- Added commented functions for views and widgets creations since most of the time, we only started writing for mobile view only

## [0.0.5]

- Now you need to confirm when using initialise command since it may accidently override your current project structure by accidently pressing it

## [0.0.4]

- Added confirmation to use whether plural or not in creating the widget

## [0.0.3]

- Added plural supports to link widget and view models correctly

## [0.0.2]

- Fixed widget creation for class names which has underscores


## [0.0.1]

- Initial release