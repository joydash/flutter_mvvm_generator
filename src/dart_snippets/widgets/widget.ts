import * as _ from 'lodash';
import { Base } from '../architecture/base';

export class Widget extends Base {

  private _dartString: string;
  private _containPlural: boolean;

  constructor(fileName: string, suffix: string, containPlural: boolean) {
    super(fileName, suffix);

    this._containPlural = containPlural;
    let classPrefixList: string[] = this.className.split('Widget');
    let classPrefix: string | undefined;
    if (!_.isEmpty(classPrefixList)) { classPrefix = _.first(classPrefixList); }

    this._dartString = `library ${fileName}_widget;

import 'package:responsive_builder/responsive_builder.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../views/${this.finalName}/${this.finalName}_view_model.dart';

part '${fileName}_mobile.dart';
part '${fileName}_tablet.dart';
part '${fileName}_desktop.dart';

// ignore: must_be_immutable
class ${this.className} extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
        mobile: _${classPrefix}Mobile(),
        desktop: _${classPrefix}Mobile(),
        tablet: _${classPrefix}Mobile(),

        //Uncomment it if you've planned to support specifically for desktop and tablet
        //desktop: _${classPrefix}Desktop(),
        //tablet: _${classPrefix}Tablet(),
    );
  }
}`;
  }

  private removeString(strVal: string, valToRemove: string) {
      return strVal.replace(valToRemove, '');
}

get finalName(): string {
  var finalName = this.removeString(this.fileName, '_item');
  if(this._containPlural)
  {
    finalName += 's';
  }

  return finalName;
}

  get dartString(): string {
    return this._dartString;
  }
}