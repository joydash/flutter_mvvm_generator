import * as _ from 'lodash';
import { Base } from '../architecture/base';

export class Tablet extends Base {

  private _dartString: string;

  constructor(fileName: string, fileNameWithoutItem: string,sfileName: string,  suffix: string) {
    super(fileName, suffix);

    this._dartString = `part of ${sfileName}_widget;
    
// ignore: must_be_immutable
class _${this.className} extends ViewModelWidget<${fileNameWithoutItem}ViewModel> {
  
  _${this.className}();

  @override
  Widget build(BuildContext context, ${fileNameWithoutItem}ViewModel viewModel) {
    return Center(
      child: Text('${sfileName}_tablet'),
    );
  }
}`;
  }

  get dartString(): string {
    return this._dartString;
  }
}