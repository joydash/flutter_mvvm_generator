import * as path from 'path';
import * as _ from 'lodash';
import { existsSync } from 'fs';
import { FileSystemManager } from './file_system_manager';
import { ViewModel } from '../dart_snippets/widgets/view_model';
import { Mobile } from '../dart_snippets/widgets/mobile';
import { Desktop } from '../dart_snippets/widgets/desktop';
import { Tablet } from '../dart_snippets/widgets/tablet';
import { Widget } from '../dart_snippets/widgets/widget';

export class WidgetFile {

    constructor(private rootPath: string, private fileName: string) {
        console.debug(`WidgetFile(rootPath: ${rootPath}, fileName: ${fileName})`);
        let folderCreated = FileSystemManager.createFolder(this.pathValue);
        if (!folderCreated) { return; }
    }

    public createResponsiveWidgets(containPlural: boolean ) {
        this.createFiles(this.snakeCasedFileName + '_widget.dart', new Widget(this.snakeCasedFileName, 'Widget', containPlural).dartString);
        this.createMobile(containPlural);
        this.createTablet(containPlural);
        this.createDesktop(containPlural);
    }

    public createView() {

    }

    public createMobile(containPlural: boolean) {
        this.createFiles(this.snakeCasedFileName + '_mobile.dart', new Mobile(this.capitalisedFileName(this.fileName, false, false), this.capitalisedFileName(this.fileName, true, containPlural), this.snakeCasedFileName, 'Mobile').dartString);
    }

    public createDesktop(containPlural: boolean) {

        this.createFiles(this.snakeCasedFileName + '_desktop.dart', new Desktop(this.capitalisedFileName(this.fileName, false, false), this.capitalisedFileName(this.fileName, true, containPlural), this.snakeCasedFileName, 'Desktop').dartString);
    }

    public createTablet(containPlural: boolean) {
        this.createFiles(this.snakeCasedFileName + '_tablet.dart', new Tablet(this.capitalisedFileName(this.fileName, false, false), this.capitalisedFileName(this.fileName, true, containPlural), this.snakeCasedFileName, 'Tablet').dartString);
    }

    public createWithViewModel(containPlural: boolean) {
        this.createFiles(this.snakeCasedFileName + '_view_model.dart', new ViewModel(this.snakeCasedFileName, 'ViewModel').dartString);
    }

    private capitalisedFileName(strVal: string, bRemoveItem: boolean, containPlural: boolean ) {
        // Since file name comes with spaces for _underscore, combine it again
        var finalName = '';
        var names = this.fileName.split(' ');
        names.forEach(name => {
            if(bRemoveItem)
            {
                if(name !== 'item' && name !== 'widget')
                {
                    finalName += this.capitalisedFirstCharacter(name);
                }
            }
            else
            {
                finalName += this.capitalisedFirstCharacter(name);

            }        
        });

        if(containPlural){
            finalName += 's';
        }

        return finalName;
    }

    private capitalisedFirstCharacter(strVal: string) {
        return strVal[0].toUpperCase() + strVal.slice(1);
    }

    private get snakeCasedFileName(): string {
        return _.snakeCase(this.fileName);
    }

    private get pathValue(): string {
        return path.join(
            this.rootPath,
            'lib',
            'widgets',
            this.snakeCasedFileName,
        );
    }

    private createFiles(fileName: string, data: string) {
        if (existsSync(path.join(this.pathValue, this.snakeCasedFileName))) {
            console.warn(`${fileName} already exists`);
            return;
        }

        FileSystemManager.createFile(this.pathValue, fileName, data);
    }
}